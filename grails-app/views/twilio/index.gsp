<%--
  Created by IntelliJ IDEA.
  User: ali
  Date: 12/5/16
  Time: 4:55 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Twilio</title>

</head>

<body>

<g:form class="form-horizontal form-without-legend" controller="twilio" action="testSendingMessage" method="post">
    <br>
    <legend>Enter details below for sms testing:</legend>

    <div class="form-group">
        <br>
        <label class="col-lg-3 control-label visible-ie8 visible-ie9">To<span class="require">*</span>
        </label>

        <div class="col-lg-8">
            <g:textField class="form-control placeholder-no-fix" type="text" placeholder="To Number"
                         name="smsToNumber"/>
        </div>

    </div>

    <div class="form-group">
        <label class="col-lg-3 control-label visible-ie8 visible-ie9">From<span class="require">*</span>
        </label>

        <div class="col-lg-8">
            <g:textField class="form-control placeholder-no-fix" type="text" placeholder="From Number"
                         name="smsFromNumber"/>
        </div>

    </div>


    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="col-lg-3 control-label visible-ie8 visible-ie9">Body<span class="require">*</span></label>

        <div class="col-lg-8">
            <g:textField class="form-control placeholder-no-fix" placeholder="Message Body"
                         name="smsMsgBody"/>
        </div>

    </div>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="col-lg-3 control-label visible-ie8 visible-ie9">Response Type</label>

        <div class="col-lg-8">
            <g:select class="form-control" name="smsResponseType" from="${['JSON', 'XML']}"/>
        </div>

    </div>

    <div class="clearfix col-md-12"></div>

    <div class="row">
        <div class="col-lg-8 col-md-offset-3 padding-left-0 padding-top-20">
            <div class="form-actions">

                <button type="submit" id="sendSms-submit-btn"
                        class="btn btn-primary uppercase">Send</button>
            </div>
        </div>
    </div>

</g:form>

<br>
<br>

<g:form class="form-horizontal form-without-legend" controller="twilio" action="testMakingCall" method="post">

    <legend>Enter details below for call testing:</legend>

    <div class="form-group">
        <br>
        <label class="col-lg-3 control-label visible-ie8 visible-ie9">To<span class="require">*</span>
        </label>

        <div class="col-lg-8">
            <g:textField class="form-control placeholder-no-fix" type="text" placeholder="To Number"
                         name="callToNumber"/>
        </div>

    </div>

    <div class="form-group">
        <label class="col-lg-3 control-label visible-ie8 visible-ie9">From<span class="require">*</span>
        </label>

        <div class="col-lg-8">
            <g:textField class="form-control placeholder-no-fix" type="text" placeholder="From Number"
                         name="callFromNumber"/>
        </div>

    </div>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="col-lg-3 control-label visible-ie8 visible-ie9">Response Type</label>

        <div class="col-lg-8">
            <g:select class="form-control" name="callResponseType" from="${['JSON', 'XML']}"/>
        </div>

    </div>

    <div class="clearfix col-md-12"></div>

    <div class="row">
        <div class="col-lg-8 col-md-offset-3 padding-left-0 padding-top-20">
            <div class="form-actions">

                <button type="submit" id="testCall-submit-btn"
                        class="btn btn-primary uppercase">Call</button>
            </div>
        </div>
    </div>

</g:form>

</body>
</html>