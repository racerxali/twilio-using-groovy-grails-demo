package demotwilio

import com.google.gson.Gson
import com.twilio.sdk.verbs.Gather
import com.twilio.sdk.verbs.Say
import com.twilio.sdk.verbs.TwiMLException
import com.twilio.sdk.verbs.TwiMLResponse
import grails.converters.JSON
import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import groovy.xml.MarkupBuilder
import groovy.xml.StreamingMarkupBuilder
import groovy.xml.XmlUtil
import org.codehaus.groovy.grails.web.json.JSONObject


class TwilioController {

    def twilioService

    def index() {
        println("==========index==========")
    }

    def testSendingMessage() {
        println("PARAMS: ${params}")
        String msg = params.smsMsgBody
        String sendTo = params.smsToNumber
        String sendFrom = params.smsFromNumber
        String authToken = grailsApplication.config.twilio.account.live.auth_token
        String sid = grailsApplication.config.twilio.account.live.sid
        String responseType = params.smsResponseType

        def testSMS = twilioService.sendSMS(msg, sendTo, sendFrom, authToken, sid)

        if (responseType == 'XML') {
            def responseAsXML = new StringWriter()
            def builder = new MarkupBuilder(responseAsXML)
            builder.notification {
                testSMS.properties.each() { key, value ->
                    "${key}" "${value}"
                }
            }

            response.setHeader("Content-Type", "text/xml")
            response.setHeader("charset", "utf-8")
            response.setContentType("text/xml")
            render responseAsXML
        } else {
            JSONObject responseAsJSON = new JSONObject();
            responseAsJSON.putAll(testSMS.properties);

            response.setHeader("Content-Type", "application/json")
            response.setHeader("charset", "utf-8")
            response.setContentType("application/json")
            render responseAsJSON.toString(2)
        }

    }

    def testMakingCall() {
        println("testMakingCall PARAMS: ${params}")
        String sendTo = params.callToNumber
        String sendFrom = params.callFromNumber
        String authToken = grailsApplication.config.twilio.account.test.auth_token
        String sid = grailsApplication.config.twilio.account.test.sid
        String responseType = params.callResponseType

        def testCall = twilioService.testCall(sendTo, sendFrom, authToken, sid)

        if (responseType == 'XML') {
            def responseAsXML = new StringWriter()
            def builder = new MarkupBuilder(responseAsXML)
            builder.notification {
                testCall.properties.each() { key, value ->
                    "${key}" "${value}"
                }
            }

            response.setHeader("Content-Type", "text/xml")
            response.setHeader("charset", "utf-8")
            response.setContentType("text/xml")
            render responseAsXML
        } else {
            JSONObject responseAsJSON = new JSONObject();
            responseAsJSON.putAll(testCall.properties);

            response.setHeader("Content-Type", "application/json")
            response.setHeader("charset", "utf-8")
            response.setContentType("application/json")
            render responseAsJSON.toString(2)
        }

    }


    def landingCall() {
        println("==================landingCall=========${params}")
        response.setHeader("Content-Type", "text/xml")
        response.setHeader("charset", "utf-8")
        response.setContentType("text/xml")

        TwiMLResponse twiMLResponse = new TwiMLResponse()
        Gather gather = new Gather()

        twilioService.menuIVR(twiMLResponse)
        render XmlUtil.serialize(twiMLResponse.toXML()).toString()
    }

    def sayMessage() {
        println "=============sayMessage===========${params}"
        String userInput = params?.Digits
        response.setHeader("Content-Type", "text/xml")
        response.setHeader("charset", "utf-8")
        response.setContentType("text/xml")
        TwiMLResponse twiMLResponse = new TwiMLResponse()
        Gather gather = new Gather()
        if (userInput.equals("1")) {
            println "You pressed keypad key 1"
            Say say = new Say("You pressed keypad key 1")
            try {
                gather.append(say)
            } catch (TwiMLException e) {
                e.printStackTrace()
            }
            try {
                twiMLResponse.append(gather)
            } catch (TwiMLException e) {
                e.printStackTrace()
            }
        } else if (userInput.equals("2")) {
            println "You pressed keypad key 2"
            Say say = new Say("You pressed keypad key 2")
            try {
                gather.append(say)
            } catch (TwiMLException e) {
                e.printStackTrace()
            }
            try {
                twiMLResponse.append(gather)
            } catch (TwiMLException e) {
                e.printStackTrace()
            }
        } else {
            Say say = new Say("It seems you enter some thing which we are not expecting. Thank you.")
            try {
                twiMLResponse.append(say)
            } catch (TwiMLException e) {
                e.printStackTrace()
            }
        }
        render XmlUtil.serialize(twiMLResponse.toXML()).toString()
    }
}
