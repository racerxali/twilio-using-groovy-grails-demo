package demotwilio

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException
import com.twilio.sdk.TwilioRestResponse;
import com.twilio.sdk.resource.factory.MessageFactory
import com.twilio.sdk.resource.factory.SmsFactory
import com.twilio.sdk.resource.instance.Account
import com.twilio.sdk.resource.instance.Call;
import com.twilio.sdk.resource.instance.Message
import com.twilio.sdk.resource.instance.Sms
import com.twilio.sdk.resource.list.AccountList
import com.twilio.sdk.verbs.Gather
import com.twilio.sdk.verbs.Say
import com.twilio.sdk.verbs.TwiMLException
import com.twilio.sdk.verbs.TwiMLResponse
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import com.twilio.sdk.resource.factory.CallFactory;

import java.util.ArrayList;
import java.util.List;

class TwilioService {

    def grailsApplication
    def grailsLinkGenerator

    final public static String TWILIO_HTTPS = 'https://'
    final public static String TWILIO_URL = 'api.twilio.com'
//    final public static String TWILIO_URL = 'api.twilio.com/2010-04-01/Accounts'


    def sendSMS(String content, String toNumber, String fromNumber, String authToken, String sid) {

//        String AUTH_TOKEN = "${grailsApplication.config.twilio.account.live.auth_token}"
        String AUTH_TOKEN = authToken
        log.info("=========AUTH_TOKEN======: ${AUTH_TOKEN}")

//        String fromMobileNumber = grailsApplication.config.twilio.phones.main

//        String fromMobileNumber = "+15005550006"
        String fromMobileNumber = fromNumber
        log.info("=========fromMobileNumber======: ${fromMobileNumber}")

        String toMobileNumber = toNumber
        log.info("=========toMobileNumber======: ${toMobileNumber}")

//        String ACCOUNT_SID = "${grailsApplication.config.twilio.account.live.sid}"
        String ACCOUNT_SID = sid
        log.info("=========ACCOUNT_SID======: ${ACCOUNT_SID}")

        String basicAuthString = ACCOUNT_SID + ':' + AUTH_TOKEN + '@'
        String urlString = "${TWILIO_HTTPS}${basicAuthString}${TWILIO_URL}"
        log.info("=========urlString======: ${urlString}")



        try {
            TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
            // Build a filter for the MessageList
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("Body", "${content}"));
            params.add(new BasicNameValuePair("To", "${toMobileNumber}"));
            params.add(new BasicNameValuePair("From", "${fromMobileNumber}"));

            SmsFactory smsFactory = client.getAccount().getSmsFactory();
            Sms sms = smsFactory.create(params);
            log.info("*************sms*************: ${sms?.properties}")

            /*MessageFactory messageFactory = client.getAccount().getMessageFactory();
            Message message = messageFactory.create(params);
            log.info("*************message*************: ${message.properties}")*/

            /*Message messageRequest = client.getAccount().getMessage("${message.sid}");
            log.info("*************messageBody*************: ${messageRequest}")
            log.info("*************messageProperties*************: ${messageRequest.properties}")

            Sms sms = client.getAccount().getSms("${message.sid}");
            log.info("*************sms*************: ${sms}")
            log.info("*************smsProperties*************: ${sms.properties}")*/

            /*String resourceLocation = "/2010-04-01/Accounts/${message.getAccountSid()}/Messages/${message.sid}.json"
            log.info("*************resourceLocation*************: ${resourceLocation}")
            log.info("*************urlString + resourceLocation*************: ${urlString + resourceLocation}")
            def request = client.request(urlString + resourceLocation, "GET", params)
            log.info("*************request*************: ${request}")
            log.info("*************request properties*************: ${request.properties}")*/

            return sms
        } catch (TwilioRestException e) {
            log.error("*************Exception Message*************: ${e.getErrorMessage()}")
            return e
        }
    }

    def testCall(String toNumber, String fromNumber, String authToken, String sid) {
//        String AUTH_TOKEN = "${grailsApplication.config.twilio.account.live.auth_token}"
        String AUTH_TOKEN = authToken
        log.info("=========AUTH_TOKEN======: ${AUTH_TOKEN}")

//        String fromMobileNumber = grailsApplication.config.twilio.phones.main

//        String fromMobileNumber = "+15005550001"
        String fromMobileNumber = fromNumber
        log.info("=========fromMobileNumber======: ${fromMobileNumber}")

//        String toMobileNumber = "+918130221148"
        String toMobileNumber = toNumber
        log.info("=========toMobileNumber======: ${toMobileNumber}")

//        String ACCOUNT_SID = "${grailsApplication.config.twilio.account.live.sid}"
        String ACCOUNT_SID = sid
        log.info("=========ACCOUNT_SID======: ${ACCOUNT_SID}")

        try {
            TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
            // Build a filter for the MessageList
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("Url", "http://demo.twilio.com/docs/voice.xml"));
            params.add(new BasicNameValuePair("To", "${toMobileNumber}"));
            params.add(new BasicNameValuePair("From", "${fromMobileNumber}"));

            CallFactory callFactory = client.getAccount().getCallFactory();
            Call call = callFactory.create(params);
            log.info("*************call*************: ${call.properties}")

            return call

        } catch (TwilioRestException e) {
            log.error("*************Exception Message*************: ${e.getErrorMessage()}")
            return e
        }
    }

    TwiMLResponse menuIVR(TwiMLResponse twiMLResponse) {
        String gatherUrl = grailsLinkGenerator.link(controller: 'twilio', action: 'sayMessage', absolute: true)
        Gather gather = new Gather()
        gather.action = gatherUrl
        gather.numDigits = 1
        Say sayDefault = new Say("Welcome to Twilio Demo by Ali Tanwir")
        Say sayMenu1 = new Say("If you want to listen Message 1. Press one")
        Say sayMenu2 = new Say("If you want to listen Message 2. Press two")
        Say sayExit = new Say("Press hash to exit")
        try {
            println("Gathering user input")
            gather.append(sayDefault)
            gather.append(sayMenu1)
            gather.append(sayMenu2)
            gather.append(sayExit)
        } catch (TwiMLException e) {
            e.printStackTrace()
        }

        try {
            twiMLResponse.append(gather)
        } catch (TwiMLException e) {
            e.printStackTrace()
        }
        return twiMLResponse
    }
}
